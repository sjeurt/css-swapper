"use strict";
var storedIndex, colorSheets;
colorSheets = [{
    color: "white",
    title: "Default",
    href: "assets/colors/default.css"
  },
  {
    color: "red",
    title: "Red",
    href: "assets/colors/red.css"
  },
  {
    color: "yellow",
    title: "Yellow",
    href: "assets/colors/yellow.css"
  },
  {
    color: "blue",
    title: "Blue",
    href: "assets/colors/blue.css"
  }
];

var ColorSwitcher = (function() {

  function initColorSwitcher(colorSheets) {
    var tempCon, colorSwitcher, controlBtn, colorSwitchs, linkHolderHtml, linkHolder, storedIndex;

    if (Object.prototype.toString.call(colorSheets) !== "[object Array]") {
      return;
    }

    tempCon = document.createDocumentFragment();

    colorSwitcher = document.createElement("div");
    colorSwitcher.classList.add("ColorSwitcher");

    controlBtn = document.createElement("button");
    controlBtn.classList.add("ColorSwitcher__control");

    colorSwitchs = document.createElement("div");
    colorSwitchs.classList.add("ColorSwitcher__switchs");

    linkHolderHtml = document.createElement("link");
    linkHolderHtml.rel = "stylesheet";
    linkHolderHtml.id = "ColorSwitcherLinkHolder";
    document.head.appendChild(linkHolderHtml);

    linkHolder = document.getElementById("ColorSwitcherLinkHolder");

    window.onload = function() {
      if (localStorage.getItem("index") === null) {
        storedIndex = 1;
      } else {
        storedIndex = localStorage.getItem("index");
      }
      linkHolder.href = colorSheets[storedIndex].href;
    };

    colorSheets.forEach(function(colorSheet, index) {
      var colorSwitch;

      if (colorSheet.color && colorSheet.title && colorSheet.href) {
        colorSwitch = document.createElement("button");

        colorSwitch.classList.add("ColorSwitcher__switch")
        colorSwitch.title = colorSheet.title;
        colorSwitch.dataset.index = index;
        colorSwitch.style.backgroundColor = colorSheet.color;

        colorSwitchs.appendChild(colorSwitch);
      }
    });

    function swapCss(e) {
      var index;

      if (e.target.nodeName !== "BUTTON") {
        return;
      }

      index = e.target.dataset.index;
      linkHolder.href = colorSheets[index].href;
    };

    function saveIndex(e) {
      var index;
      index = e.target.dataset.index;
      localStorage["index"] = index;
    };

    colorSwitchs.addEventListener("click", saveIndex, false);
    colorSwitchs.addEventListener("click", swapCss, false);
    controlBtn.addEventListener("click", function(event) {
      event.target.parentElement.classList.toggle("ColorSwitcher--open");

      return false;
    });

    colorSwitcher.appendChild(controlBtn);
    colorSwitcher.appendChild(colorSwitchs);
    tempCon.appendChild(colorSwitcher);
    document.body.appendChild(tempCon);
  }

  return {
    init: initColorSwitcher
  };
})();

ColorSwitcher.init(colorSheets);